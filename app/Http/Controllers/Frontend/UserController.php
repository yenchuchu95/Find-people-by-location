<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repositories\Api\User\UserRepositoryInterface;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
//use Tymon\JWTAuth\JWTAuth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Validator;
use Response;
use App\Frontend\User;

class UserController extends Controller {
    protected $user;

    public function __construct(UserRepositoryInterface $user) {
        $this->user = $user;
    }

    public function index() {
        $user = $this->user->getAllUser();
        return $user;
    }

}

?>